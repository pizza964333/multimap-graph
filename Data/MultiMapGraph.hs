{-# LANGUAGE GADTs #-}

module Data.MultiMapGraph where

import Data.Int
import Data.Word

data Term
data Cross
data Cycle

data Hyper a where
     HTerm  :: a -> Hyper Term -> Hyper Term -> Hyper Term
     HCross ::      Hyper Term -> Hyper Term -> Hyper Cross

data Ring a where
     RCycle :: Ring Term -> Ring Term -> Ring Term -> Ring Cycle
     RTerm  :: Ring Term -> Ring Term

data Flat a where

walkHyper :: Bool -> [Bool] -> Hyper Cross -> Hyper Cross
walkHyper True (True :pt) (HCross (HTerm v a b) c) = walkHyper True  pt $ HCross a $ HTerm v b c
walkHyper True (False:pt) (HCross (HTerm v a b) c) = walkHyper False pt $ HCross (HTerm v c a) b

type PHb = Int64
type P2d = (Word64,Word64)
type PCr = Word64

type Point = (PHb,PCr,P2d)
type Range = (Point,Point)

data Graph a = Graph [(Double,a)] [[(Double,Int)]] (HRFtree Int)

data HRFtree a
 = Empty
 | Leaf { getPoint :: Point, getElem   :: a }
 | Node { getRange :: Range, getChilds :: [HRFtree a] }

unionDistinct :: HRFtree a -> HRFtree a -> HRFtree a
unionDistinct Empty{} t = t
unionDistinct t Empty{} = t
unionDistinct t1@Leaf{} t2@Leaf{} = undefined

mkRange :: Point -> Point -> Range
mkRange = undefined

rangeP2d :: P2d -> P2d -> (P2d,P2d)
rangeP2d (a,b) (c,d) = ((min a c,min b d),(max a c,max b d))

isIntersectP2d :: (P2d,P2d) -> (P2d,P2d) -> Bool
isIntersectP2d ((a,b),(c,d)) ((e,f),(g,h)) = (a <= e && e <= c && b <= f && f <= d) || (a <= g && g <= c && b <= h && h <= d)

rangePCr :: PCr -> PCr -> (PCr,PCr)
rangePCr a b = (min a b, max a b)

mergePCr :: (PCr,PCr) -> (PCr,PCr) -> Either Bool (PCr,PCr)
mergePCr (a,b) (c,d) | not (e || f) && g = if isOverflow (a-d) then Left  True  else Right (a,d)
                     | not (e || f) && h = if isOverflow (b-c) then Right (b,c) else Left True
                     |   e && not f && i = if isOverflow (d-b) then Right (d,b) else Left True
                     |   e && not f && j = if isOverflow (a-c) then Right (a,c) else Left True
                     |   not e && f && k = if isOverflow (b-d) then Right (b,d) else Left True
                     |   not e && f && l = if isOverflow (c-a) then Right (c,a) else Left True
                     |       e && f      = Right (max a c, min b d)
                     | otherwise         = Left False
 where

  e = isOverflow $ a - b
  f = isOverflow $ c - d

  g = b > c
  h = d > a

  i = c < a
  j = d > b

  k = a < c
  l = b > d

isOverflow a = a > 9223372036854775807



























