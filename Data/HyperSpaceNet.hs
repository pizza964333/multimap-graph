{-# LANGUAGE GADTs, MagicHash #-}

module Data.HyperSpaceNet where

import Data.Word
import GHC.Integer.Logarithms
import GHC.Exts
import Data.Bits
import System.Random
import System.IO.Unsafe

type Path  =  Integer
type Dist  =  Word8
type Range = (Path, Dist) -- path and hyper circle radius

data Hyper a = Hyper   (HTerm a) (HTerm a)
data HTerm a = HTerm a (HTerm a) (HTerm a)

walk :: Path -> Hyper a -> Hyper a
walk p (Hyper (HTerm v a b) c) | isL p && isL (dr1 p) = walk (setL $ dr1 p) $ Hyper a (HTerm v b c)
walk p (Hyper (HTerm v a b) c) | isL p && isR (dr1 p) = walk (setR $ dr1 p) $ Hyper (HTerm v c a) b

-- merge :: Range -> Range -> Range
merge (p1,r1) (p2,r2) | r < d = Nothing
 where
  d = dist p1 p2
  r = r1 + r2

{-
  m = middle p1 p2
-}


randomPath :: IO Integer
randomPath = randomRIO (2^8,2^24)

paths = map (\_ -> unsafePerformIO randomPath) [0..]

dist :: Path -> Path -> Dist
dist a b = toEnum $ e + f
 where
  (c,d) = cutSame (a,b)
  e = ilog c
  f = ilog d


middle :: Path -> Path -> Range
middle a b | ilog a < ilog b = middle b a
           | a /= c          = (g,toEnum i)
           | a == c          = (dr1 h,toEnum i)
 where
  (c,d) = cutSame (a,b)
  e = half $ path c d
  i = ilog e
  g = reduceBy e a
  h = reduceBy e (a*2)

reduceBy p rt = if isL rt then d*2 else d*2+1
 where
  a = rev $ inv $ dr1 rt
  (b,c) = cutSame (p,a)
  d = rev $ inv c

half a = (a .&. c) + (2^b)
 where
  b = ilog a `div` 2
  c = (2^b) - 1

roundUp a | a `mod` 2 == 0 = a
          | otherwise      = a+1

path a b = xor c e
 where
  c = rev $ inv a
  d = ilog a
  e = b * (2^(d))

cutSame (1,b) = (1,b)
cutSame (a,1) = (a,1)
cutSame (a,b) | isL a && isL b = cutSame (dr1 a, dr1 b)
cutSame (a,b) | isR a && isR b = cutSame (dr1 a, dr1 b)
cutSame (a,b) = (a,b)

rev a = fromBits 0 c
 where
  b = reverse $ toBits a
  c = 1 : (reverse $ tail b)

inv a = xor a d
 where
  c = ilog a
  d = (2^c)-1

toBits 1 = [1]
toBits a = mod a 2 : toBits (div a 2)

fromBits n [] = n
fromBits n (1:s) = fromBits (n*2+1) s
fromBits n (0:s) = fromBits (n*2  ) s

chg p | p `mod` 2 == 0 = p + 1
      | p `mod` 2 == 1 = div p 2 * 2

drp n p = p `div` (2^n)

dr1 = drp 1
dr2 = drp 2

isL p = p `mod` 2 == 0
isR p = p `mod` 2 == 1

setL p = p * 2
setR p = p * 2 + 1

ilog x = I#(integerLog2# x)

